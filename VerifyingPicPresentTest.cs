using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using NUnit.Framework;
[TestFixture]
public class VerifyingPicPresentTest {
  private IWebDriver driver;
  public IDictionary<string, object> vars {get; private set;}
  private IJavaScriptExecutor js;
  [SetUp]
  public void SetUp() {
    driver = new ChromeDriver();
    js = (IJavaScriptExecutor)driver;
    vars = new Dictionary<string, object>();
  }
  [TearDown]
  protected void TearDown() {
    driver.Quit();
  }
  [Test]
  public void verifyingPicPresent() {
    driver.Navigate().GoToUrl("https://www.google.com/");
    driver.Manage().Window.Size = new System.Drawing.Size(1920, 1080);
    driver.FindElement(By.Name("q")).Click();
    driver.FindElement(By.Name("q")).SendKeys("cat picture wikidata");
    driver.FindElement(By.CssSelector(".QCzoEc > svg")).Click();
    driver.FindElement(By.Name("q")).SendKeys(Keys.Enter);
    driver.FindElement(By.Name("q")).SendKeys("cat picture");
    driver.FindElement(By.LinkText("Зображення")).Click();
    driver.FindElement(By.CssSelector(".isv-r:nth-child(2) .rg_i")).Click();
    try {
      Thread.Sleep(5000);
    } catch (Exception e) {
      Console.WriteLine("{0} Exception caught.", e);
    }
    var elements = driver.FindElements(By.XPath("//img[@src=\'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4d/Cat_November_2010-1a.jpg/800px-Cat_November_2010-1a.jpg\']"));
    Assert.True(elements.Count > 0);
  }
}

